<?php
// Enqueue scripts for masonry and infinite scroll
add_action('wp_enqueue_scripts', 'genesiscards_load_scripts_frontpage');
function genesiscards_load_scripts_frontpage() {
    wp_enqueue_script( 'infinite_scroll',  get_stylesheet_directory_uri() . '/js/jquery.infinitescroll.min.js', array( ), null, true );
    wp_enqueue_script( 'masonry-isotope',  get_stylesheet_directory_uri() . '/js/masonry-isotope.js', array('infinite_scroll'), null, true );
    wp_enqueue_script( 'masonry-init',  get_stylesheet_directory_uri() . '/js/init-masonry.js', array('jquery'), null, true );
    wp_enqueue_script( 'jquery-masonry', array( 'jquery' ), true );
}

// Add custom post class to posts
add_filter('post_class', 'genesiscards_custom_post_class');
function genesiscards_custom_post_class($classes) {
  $new_class = 'brick';

  $classes[] = esc_attr($new_class);

  return $classes;
}

//* Handles the masonry design
function genesiscards_masonry() { 
  
    // $paged   = get_query_var( 'paged', 1 );
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $posts_per_page = (genesis_get_option('blog_cat_num')) ? genesis_get_option('blog_cat_num') : 9;
    $search_query = get_search_query();
 
    $cat_term = get_query_var( 'cat' );

    $tag_term = get_query_var( 'tag' );

    $args = ( array(
        'post_type'         => 'post',
        'order'             => 'desc',
        'order_by'          => 'date',
        'paged'             => $paged,
        'posts_per_page'    => $posts_per_page,
        's'                 => $search_query,
        )
    );

    if ( $cat_term != '' ) {
        $args['category__in'] = $cat_term;
    }

    if ( $tag_term != '' ) {
        $args['tag'] = $tag_term;
    }

    echo '<section class="blog" id="container">';
    genesis_custom_loop( $args );
    echo '</section>';

    // do_action( 'genesis_after_endwhile' );
}


// Add init infinite scroll
add_action( 'wp_footer', 'custom_infinite_scroll_js',100);
function custom_infinite_scroll_js() {
    ?>
    <script>
    var infinite_scroll = {
        loading: {
            msgText: "<?php _e( 'Loading the next set of posts...', 'genesiscards' ); ?>",
            finishedMsg: "<?php _e( 'All posts loaded.', 'genesiscards' ); ?>"
        },
        "nextSelector":".pagination-next a",
        "navSelector":".pagination",
        "itemSelector":".brick",
        "contentSelector":"#container",
        "behavior": "masonry"
    };
    jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll );
    </script>
    <?php
}

//* Custom featured post image
function genesiscards_do_post_image() {
    global $post;
    //* Output featured image
	$image_args = array(
		'size'  => 'masonry-featured-image',
        'attr'   => array(  
            'class' => 'masonry-featured-image centered',
            'alt'   => $post->post_title,
            'title' => $post->post_title,
        ),
	);

    if ( has_post_thumbnail() ) {
        echo '<a class="featured-post-link" href="' . get_permalink() . '" title="' . get_the_title() . '"><i class="overlay"></i>' . genesis_get_image( $image_args ) . '</a>';
    }
}

//* Custom featured post image
function genesiscards_do_post_image_fancy() {
    global $post;
    //* Output featured image
    $image_url_args = array(
        'format' => 'url',
		'size'  => 'full',
        'attr'   => array(  
            'alt'   => $post->post_title,
            'title' => $post->post_title,
        ),
	);

	$image_args = array(
        'format' => 'html',
		'size'  => 'masonry-featured-image',
        'attr'   => array(  
            'class' => 'masonry-featured-image centered',
            'alt'   => $post->post_title,
            'title' => $post->post_title,
        ),
	);

    if ( has_post_thumbnail() ) {
        echo '<a class="featured-post-link image fancybox" href="' . genesis_get_image( $image_url_args ) . '" title="' . get_the_title() . '"><i class="overlay"></i>' . genesis_get_image( $image_args ) . '</a>';
    }
}

//* Customize the post meta function
add_filter( 'genesis_post_meta', 'sp_post_meta_filter' );
function sp_post_meta_filter($post_meta) {
if ( !is_page() ) {
	$post_meta = '[post_categories before="What Under: "] [post_tags before="Tagged: "]';
	return $post_meta;
}}

//* Filters how post-meta is handled
function genesiscards_post_info() {
    $post_info = '<div class="entry-meta">';
    $post_info .= '<div class="one-half first">';
    $post_info .= do_shortcode('[post_date]');
    $post_info .= '</div>';
    $post_info .= '<div class="one-half alignright">';
    $post_info .= do_shortcode('[post_author]');
    $post_info .= '</div>';
    $post_info .= '<div class="clearfix"></div>';
    $post_info .= '</div>';
    
	echo $post_info;
}

//* Filters how post-meta is handled
function genesiscards_post_meta() {
    $post_meta = '<p class="entry-meta">';
    $post_meta .= do_shortcode('[post_categories sep="," before=""]');
    $post_meta .= '</p>';
    
	echo $post_meta;
}

//* Modify the Genesis content limit read more link
add_filter( 'get_the_content_more_link', 'genesiscards_masonry_read_more_link_filter' );
function genesiscards_masonry_read_more_link_filter() {
	return '...';
}

//* Add a Read more to the bottom of a brick
function genesiscards_read_more_footer() {
    echo '<a class="more-link" href="' . get_permalink() . '">Read More</a>';
}

//* Get the external link that's in the link format post
function genesiscards_get_link_post() {
    global $post;

    $link = get_post_meta( $post->ID, '_post_link_url', true);

    if ( $link == '' )
        return false;
    //if ( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches ) )
    //    return false;
 
    return esc_url_raw( $link );
}

function genesiscards_post_format_link_title() {
    $ext_link = genesiscards_get_link_post();
    echo '<h2 class="entry-title" itemprop="headline"><a href="' . $ext_link . '" rel="bookmark">' . get_the_title() . '</a></h2>';
    echo '<p class="link-disclaimer">This is an external link and will take you to a new page.</p>';
}


//* Check post format and change output based on format
function genesiscards_check_post_format() {
    // cycle through post formats
    //* QUOTE FORMAT
    if ( ( get_post_format() == 'quote' || get_post_format() == 'video' ) && !is_single() ) {
        //* Handle Header
        remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
        remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
        remove_action( 'genesis_entry_header', 'genesiscards_post_meta', 3 );
        remove_action( 'genesis_entry_header', 'genesiscards_do_post_image_fancy', 2 );
        remove_action( 'genesis_entry_header', 'genesiscards_do_post_image', 2 );
        remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

        //* Handle Content
        remove_action( 'genesis_entry_content', 'genesis_do_post_title', 4 );
        remove_action( 'genesis_entry_content', 'genesiscards_post_format_link_title' );
        remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
        remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
        add_action( 'genesis_entry_content', 'the_content' );

        //* Handle Footer
        remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
        remove_action( 'genesis_entry_footer', 'genesiscards_read_more_footer' );
    } elseif ( get_post_format() == 'link' && !is_single() ) {
        //* LINK FORMAT
        //* Handle Header
        add_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 1 );
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
        remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
        remove_action( 'genesis_entry_header', 'genesiscards_post_meta', 3 );
        remove_action( 'genesis_entry_header', 'genesiscards_do_post_image_fancy', 2 );
        remove_action( 'genesis_entry_header', 'genesiscards_do_post_image', 2 );
        add_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 10 );

        //* Handle Content
        remove_action( 'genesis_entry_content', 'genesis_do_post_title', 4 );
        remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
        remove_action( 'genesis_entry_content', 'the_content' );
        remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
        add_action( 'genesis_entry_content', 'genesiscards_post_format_link_title' );
        //* Handle Footer
        remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
        remove_action( 'genesis_entry_footer', 'genesiscards_read_more_footer' );
    } elseif ( get_post_format() == 'image' && !is_single() ) {
        //* IMAGE FORMAT
        //* Handle Header
        add_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 1 );
        add_action( 'genesis_entry_header', 'genesiscards_do_post_image_fancy', 2 );
        remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
        remove_action( 'genesis_entry_header', 'genesiscards_do_post_image', 2 );
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
        remove_action( 'genesis_entry_header', 'genesiscards_post_meta', 3 );
        remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
        add_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 10 );

        //* Handle Content
        remove_action( 'genesis_entry_content', 'genesis_do_post_title', 4 );
        remove_action( 'genesis_entry_content', 'genesiscards_post_format_link_title' );
        remove_action( 'genesis_entry_content', 'the_content' );
        remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

        //* Handle Footer
        remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
        remove_action( 'genesis_entry_footer', 'genesiscards_read_more_footer' );
    } else {
        //* STANDARD FORMAT POST
        //* Handle Header
        add_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 1 );
        add_action( 'genesis_entry_header', 'genesiscards_do_post_image', 2 );
        add_action( 'genesis_entry_header', 'genesiscards_post_meta', 3 );
        remove_action( 'genesis_entry_header', 'genesiscards_do_post_image_fancy', 2 );
        remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
        add_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 10 );

        //* Handle Content
        remove_action( 'genesis_entry_content', 'genesiscards_post_format_link_title' );
        remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );        
        remove_action( 'genesis_entry_content', 'the_content' );
        add_action( 'genesis_entry_content', 'genesis_do_post_title', 4 );
        add_action( 'genesis_entry_content', 'genesis_do_post_content' );

        //* Handle Footer
        remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
        add_action( 'genesis_entry_footer', 'genesiscards_read_more_footer' );
    }
}

//* CUSTOM MARKUP FOR MASONRY STYLE
// Masonry loop
remove_action( 'genesis_before_loop', 'genesis_do_search_title' );
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesiscards_masonry_content_area', 'genesiscards_masonry', 30 );

//* Remove the entry header markup (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

//* If needed, remove the taxonomy headline description
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );

//* Checkstep for post formats
add_action( 'genesis_before_entry', 'genesiscards_check_post_format' );

remove_action( 'genesis_before_loop', 'genesis_do_search_title' );
?>