<?php
add_action('edit_form_after_title', 'genesiscards_link_form_field');
add_action('save_post', 'genesiscards_link_form_field_save');

function genesiscards_link_form_field( $post ) {
    if ( $post->post_type != 'post') return;
    
    $format = get_post_format($post->ID);
    
    if ( $format != 'link' ) return;
?>
<div id="urlwrap">
    <p>
        <label for="post-link-url"><strong>URL:</strong></label>
        <input type="text" class="large-text" name="_post_link_url" size="30" value="<?php echo get_post_meta($post->ID, '_post_link_url', true); ?>" id="post-link-url" autocomplete="off" />
    </p>
</div>
<?php
}

function genesiscards_link_form_field_save( $postid ) {
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE );
    if ( isset($_POST['_post_link_url']) ) {
        if (( strpos($_POST['_post_link_url'], 'http://') !== 0 ) && (strpos($_POST['_post_link_url'], 'https://') !== 0 ))
        $_POST['_post_link_url'] = 'http://' . $_POST['_post_link_url'];
        $url = filter_var($_POST['_post_link_url'], FILTER_VALIDATE_URL) ? $_POST['_post_link_url'] : '';
    update_post_meta($postid, '_post_link_url', $url);
    }
}
?>