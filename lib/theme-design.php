<?php
/** Reposition header outside main wrap */
remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'genesis_header_markup_close', 15 ) ;

add_action( 'genesis_before', 'genesis_header_markup_open', 5 );
add_action( 'genesis_before', 'genesis_do_header' );
add_action( 'genesis_before', 'genesis_header_markup_close', 15 );

/** 
  * Reposition the Primary navigation at the top of the DOM.
  * @author Calvin Koepke
  * @link http://www.calvinkoepke.com/add-a-mobile-friendly-off-canvas-menu-in-genesis
*/
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_before', 'genesis_do_nav', 1 );

/**
  * Add the menu to the .site-header, but hooking right before the genesis_header_markup_close action.
  * @author Calvin Koepke (@cjkoepke)
  * @link http://www.calvinkoepke.com/add-a-mobile-friendly-off-canvas-menu-in-genesis
*/
add_action( 'genesis_before', 'ck_menu_button', 14 );
function ck_menu_button() {
	
  //* Only add the Menu button if a primary navigation is set. You can switch this for whatever menu you are dealing with
  if ( has_nav_menu( "primary" ) ) {
    
    echo '<a alt="Toggle Menu" href="#" class="menu-btn right small"><span class="dashicons dashicons-menu"></span></a>';
  
  }
  
}
//* Add the "Close Navigation" text and widget area to the Primary menu output.
add_filter( 'genesis_nav_items', 'genesiscards_nav_extras', 10, 2 );
add_filter( 'wp_nav_menu_items', 'genesiscards_nav_extras', 10, 2 );
function genesiscards_nav_extras($menu, $args) {
  
  $extras = '<a href="#" class="close-btn"><em>' . __( 'Close Navigation', 'genesis-cards' ) . '</em> <span class="dashicons dashicons-no"></span></a>';
  $nav_widget = '';

  if ( is_active_sidebar( 'nav-widget' ) ) {
      ob_start();
      genesis_widget_area ('nav-widget', array(
        'before' => '<section id="nav-widget" class="nav-widget"><div class="widget-wrap">',
        'after' => '</div></section>',
	) );
      $nav_widget = ob_get_clean();
  }
  
  if ( $args->theme_location == "primary" ) {
    
    return $extras . $menu . $nav_widget;
  
  } else {
  
    return $menu;
  
  }
  
}

/** 
  * Add the overlay div that will be used for clicking out of the active menu.
  * @author Calvin Koepke (@cjkoepke)
  * @link http://www.calvinkoepke.com/add-a-mobile-friendly-off-canvas-menu-in-genesis
*/
add_action( 'genesis_before', 'ck_site_overlay', 2 );
function ck_site_overlay() {
  
  echo '<div class="site-overlay"></div>';
}

//* Hook site avatar before site title
add_action( 'genesis_site_title', 'genesiscards_site_gravatar', 5 );
function genesiscards_site_gravatar() {

	$header_image = get_header_image() ? '<img alt="" src="' . get_header_image() . '" />' : get_avatar( get_option( 'admin_email' ), 96 );
	printf( '<div class="site-avatar"><a href="%s">%s</a></div>', home_url( '/' ), $header_image );

}
//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'genesiscards_author_box_gravatar_size' );
function genesiscards_author_box_gravatar_size( $size ) {
	return '96';
}

//* Change the footer text
add_filter('genesis_footer_creds_text', 'genesiscards_footer_creds_filter');
function genesiscards_footer_creds_filter( $creds ) {
	$creds = '[footer_copyright] &middot; <a href="' . site_url() . '">' . get_bloginfo( 'name' ) . '</a> &middot; Built on the <a href="http://www.studiopress.com/themes/genesis" title="Genesis Framework">Genesis Framework</a>';
	return $creds;
}

//* Reposition Page Title
add_action( 'genesis_before', 'genesiscards_post_page_title' );
function genesiscards_post_page_title() {
    //* If not on the home page, present the title bar
    remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
    remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
    remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
    remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

    if ( is_page() && !is_page_template() ) {
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_open', 1 );
        add_action( 'genesis_after_header', 'genesis_do_post_title', 2 );
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_close', 10 );
        
    } elseif ( is_category() ) {
        remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_open', 1 );
        add_action( 'genesis_after_header', 'genesis_do_taxonomy_title_description', 2 );
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_close', 10 );

    } elseif ( is_search() ) {
        remove_action( 'genesis_before_loop', 'genesis_do_search_title' );
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_open', 1 );
        add_action( 'genesis_after_header', 'genesis_do_search_title', 2 );
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_close', 10 );

    } elseif ( is_single() ) {
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_open', 1 );
        add_action( 'genesis_after_header', 'genesis_do_post_title', 2 );
        add_action( 'genesis_after_header', 'genesis_post_info', 3 );
        add_action( 'genesis_after_header', 'genesis_post_meta', 4 );
        add_filter( 'genesis_post_info', 'genesiscards_single_post_info_filter' );
        add_filter( 'genesis_post_meta', 'genesiscards_single_post_meta_filter' );
        add_action( 'genesis_after_header', 'genesiscards_do_title_bar_close', 10 );
        genesiscards_remove_entry_footer();
    } 
}

//* Open #title_bar
function genesiscards_do_title_bar_open() {
    // attempt to get post thumbnail
    $post_thumbnail_id = get_post_thumbnail_id();

    //* Output featured image
    $image_url_args = array(
        'format' => 'url',
		'size'  => 'full',
	);

    echo '<!-- start #title_bar -->';
    // if there is a post thumbnail
    if ( $post_thumbnail_id != NULL ) {
        echo '<div id="title_bar" style="background-image: url('. genesis_get_image( $image_url_args ) . '); background-position: center center; background-size: cover; background-repeat: no-repeat; background-blend-mode: overlay;"><div class="wrap">';
    }
    else
    {
        echo '<div id="title_bar"><div class="wrap">';
    }
}

//* Close #title_bar
function genesiscards_do_title_bar_close() {
    echo '</div></div> <!-- end #title_bar -->';
}

function genesiscards_remove_entry_footer() {
    remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
    remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
    remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
}

//* Customize the post info function
function genesiscards_single_post_info_filter($post_info) {
if ( !is_page() ) {
	$post_info = '[post_author_posts_link] [post_comments] [post_edit]';
	return $post_info;
}}

//* Customize the post meta function
function genesiscards_single_post_meta_filter($post_meta) {
    if ( !is_page() ) {
	    $post_meta = '[post_categories before=""] [post_tags before=""]';
	    return $post_meta;
    }
}

/**
 * Echo the title with the search term.
 *
 * @since 1.9.0
 */
function genesis_do_search_title() {

	$title = sprintf( '<div class="archive-description"><h1 class="archive-title">%s %s</h1></div>', apply_filters( 'genesis_search_title_text', __( 'Search Results for:', 'genesis' ) ), get_search_query() );

	echo apply_filters( 'genesis_search_title_output', $title ) . "\n";

}
?>