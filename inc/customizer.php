<?php 
//* Registers necessary custom settings for the theme customizer
function genesiscards_customize_register( $wp_customize ) {
    // add color scheme section to settings
    $wp_customize->add_section(
        'genesiscards_color_scheme_section',
        array(
            'title' => 'Genesis Cards Color Scheme',
            'description' => 'This section handles the various color settings for the Genesis Cards theme. You can select colors to create your own unique look.',
            'priority' => 35,
        )
    );

    // main color ( site title, h1, h2, h3, h4, h5, h6, nav background, footer background, page title background )
    $txtcolors[] = array(
        'slug'=>'primary_color_scheme', 
        'default' => '#000',
        'label' => 'Primary Color'
    );
 
    // link color ( a, navigation hover )
    $txtcolors[] = array(
        'slug'=>'link_color', 
        'default' => '#0000FF',
        'label' => 'Link Color'
    );
 
    // link color (hover, active)
    $txtcolors[] = array(
        'slug'=>'link_color_hover', 
        'default' => '#000',
        'label' => 'Link Color (on hover)'
    );

    // close navigation color hover
    $txtcolors[] = array(
        'slug'=>'close_navigation_color', 
        'default' => '#0000FF',
        'label' => 'Close Navigation Color (on hover)'
    );
 
    // link card color
    $txtcolors[] = array(
        'slug'=>'link_card_color', 
        'default' => '#0000FF',
        'label' => 'Link Card: Background Color'
    );

    // quote card color
    $txtcolors[] = array(
        'slug'=>'quote_card_color', 
        'default' => '#00FFFF',
        'label' => 'Quote Card: Background Color'
    );

    // quote card text color
    $txtcolors[] = array(
        'slug'=>'quote_card_text_color', 
        'default' => '#000',
        'label' => 'Quote Card: Text Color'
    );

    // add the settings and controls for each color
    foreach( $txtcolors as $txtcolor ) {
 
        // SETTINGS
        $wp_customize->add_setting(
            $txtcolor['slug'], array(
                'default' => $txtcolor['default'],
                'type' => 'option', 
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );

        // CONTROLS
        $wp_customize->add_control(
            new WP_Customize_Color_Control(
                $wp_customize,
                $txtcolor['slug'], 
                array('label' => $txtcolor['label'], 
                'section' => 'colors',
                'settings' => $txtcolor['slug'])
            )
        );
    }
}
add_action( 'customize_register', 'genesiscards_customize_register' );
?>