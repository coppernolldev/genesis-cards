GENESIS CARDS THEME
http://brandoncoppernoll.com/
INSTALL
1. Upload the Genesis Cards theme folder via FTP to your wp-content/themes/ directory. (The Genesis parent theme needs to be in the wp-content/themes/ directory as well.)
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Genesis Cards theme.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking. Under Blog Page Template, set Posts Per Page to your desired number of cards.RECOMMENDED PLUGINS1. Easy Fancybox (https://wordpress.org/plugins/easy-fancybox/) - This supports the lightbox functionality of the images in the cards system.SUPPORT
Please contact brandon@brandoncoppernoll.com for support.CHANGELOG= 1.4.2 =* Minor performance tweak.= 1.4.1 = * Added featured image background functionality to posts and pages (note: IE may not full support)= 1.4.0 =* Updated hover for menu button on desktop* Added new widget area at the bottom of the primary navigation - multiple widgets can be added to this space= 1.3.8 =* Updated hover for menu button on mobile to match link colors set by customizer* Updated nav primary background color to match primary color set by customizer= 1.3.7 =* Fix bug with link post types for featured images and fancy box.= 1.3.6 =* Fixing link post types to not include featured image or content.= 1.3.5 = * Official release for Github Updater Support and Bitbucket= 1.3.2 = * Made corrections to Bitbucket/Github Updater Support= 1.3.1 = * Tweaks to Bitbucket/Github Updater Support= 1.3.0 =* Added Bitbucket/Github support
= 1.2.0 =

* Added color options to Colors section of Theme Customizer
* Streamlined ::before elements in Quote Card and meta information

= 1.1.0 =
* Updated Genesis Custom Loop $args to fix issue with pagination* Updated styling for menu and blockquotes in card style* Added default fancybox support for image cards* Remove Genesis pagination (handled by infinite scroll)* Added Readme.txt