jQuery(function ($) {
    var stickyNavTop = $('.site-header').offset().top;

    var stickyNav = function () {
        var scrollTop = $(window).scrollTop();
        if (window.innerWidth > 600) {
            if (scrollTop > stickyNavTop) {
                $('.site-header').addClass('stickymenu');
            } else {
                $('.site-header').removeClass('stickymenu');
            }
        } else {
            if (scrollTop > stickyNavTop) {
                $('.site-header').addClass('stickymenu top');
            } else {
                $('.site-header').removeClass('stickymenu top');
            }
        }
    };

    stickyNav();

    $(window).scroll(function () {
        stickyNav();
    });
}); 