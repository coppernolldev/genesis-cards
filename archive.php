<?php

//* Setup Masonry
include_once( get_stylesheet_directory() . '/lib/masonry-design.php' );

/*
 * @author  Frank Schrijvers
 * @link    https://www.wpstud.io
 */

// Build the page
get_header();
do_action( 'genesiscards_masonry_content_area' );
get_footer();

genesis();