<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-design.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Genesis Cards' );
define( 'CHILD_THEME_URL', 'http://brandoncoppernoll.com/' );
define( 'CHILD_THEME_VERSION', '1.4.3' );

//* Enqueue Google Fonts
add_action( 'wp_enqueue_scripts', 'genesiscards_enqueue_scripts' );
function genesiscards_enqueue_scripts() {
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic', array(), CHILD_THEME_VERSION );
    wp_enqueue_script( 'offcanvas-menu', get_stylesheet_directory_uri() . '/js/offcanvas-nav.js', array( 'jquery' ), CHILD_THEME_VERSION );
    wp_enqueue_script( 'sticky-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/sticky-menu.js', array( 'jquery' ), CHILD_THEME_VERSION );
    wp_enqueue_style( 'dashicons' );

}

//* Unregister layout settings
genesis_unregister_layout( 'sidebar-content' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

//* Register widget areas
//* Register after post widget area
genesis_register_sidebar( array(
	'id'            => 'nav-widget',
	'name'          => __( 'Navigation Widget', 'genesis-cards' ),
	'description'   => __( 'This is the widget area that appears at the bottom of your navigation.', 'genesis-cards' ),
) );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

//* Unregister sidebar
unregister_sidebar( 'sidebar-alt' );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( 'headings', 'drop-down-menu',  'search-form', 'skip-links', 'rems' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 2 );

/* ADD IMAGE SIZES */
add_image_size( 'masonry-featured-image', 600, 450, TRUE );

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Add support for post formats
add_theme_support( 'post-formats', array(
	'image',
	'link',
	'quote',
) );

//* check to see if in admin
if ( is_admin() ) {
    include_once( get_stylesheet_directory() . '/lib/admin/post-link.php' );
}

//* Include the customizer
include_once( 'inc/customizer.php' );

//* Include customizer codes/colors
function genesiscards_customizer_css() {
    ?>
    <style type="text/css" media="screen">
        body, h1, h2, h3, h4, h5, h6, 
        .site-title, .site-title a, .site-title a:hover, .site-title a:focus,
        .entry-title a, .sidebar .widget-title a,
        .brick .entry-header .entry-meta a {
            color: <?php echo get_option( 'primary_color_scheme' ); ?>; 
        }

        #title_bar, .site-footer, .nav-primary { background-color: <?php echo get_option( 'primary_color_scheme' ); ?>; }

        .menu-btn:hover, .off-canvas-active .menu-btn {
            color: <?php echo get_option( 'primary_color_scheme' ); ?>;
        }
        
        a, .brick .entry-header .entry-meta a:hover, 
        .entry-title a:hover, .sidebar .widget-title a:hover { 
            color: <?php echo get_option( 'link_color' ); ?>; 
        }
        a:hover { color: <?php echo get_option( 'link_color_hover' ); ?>; }
        
        .brick a.featured-post-link, .brick .entry-footer a.more-link { background-color: <?php echo get_option( 'link_color' ); ?>; }
        .brick a.featured-post-link:hover, .brick .entry-footer a.more-link:hover { background-color: <?php echo get_option( 'link_color_hover' ); ?>; }
        
        .brick.format-link { background-color: <?php echo get_option( 'link_card_color' ); ?>; }
        .brick.format-quote { background-color: <?php echo get_option( 'quote_card_color' ); ?>; color: <?php echo get_option( 'quote_card_text_color' ); ?>; }

        .nav-primary .close-btn:hover { color: <?php echo get_option( 'close_navigation_color' ); ?>; }
        .genesis-nav-menu .menu-item a:hover { background-color: <?php echo get_option( 'link_color' ); ?>; }
        .nav-primary a:hover, .nav-primary .current-menu-item > a, 
        .nav-primary .sub-menu .current-menu-item > a:hover { color: <?php echo get_option( 'link_color' ); ?>; }

        .brick a.featured-post-link .overlay {
            background: rgba(<?php echo hex2rgb( get_option( 'link_color' ) ); ?>,0.6);
        }

        @media only screen and (max-width: 800px) {
            .menu-btn {
                background-color: <?php echo get_option( 'link_color' ); ?>;
                color: #fff;
            }

            .menu-btn:hover { color: #fff; }
        }
    </style>
    <?php
}
add_action( 'wp_head', 'genesiscards_customizer_css' );

//* Convert Hex to RGB (source: https://css-tricks.com/snippets/php/convert-hex-to-rgb/)
function hex2rgb( $colour ) {
        if ( $colour[0] == '#' ) {
                $colour = substr( $colour, 1 );
        }
        if ( strlen( $colour ) == 6 ) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
        } elseif ( strlen( $colour ) == 3 ) {
                list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
        } else {
                return false;
        }
        $r = hexdec( $r );
        $g = hexdec( $g );
        $b = hexdec( $b );

        return $r . ',' . $g . ',' . $b; // Modified by BKC to return as string
        // return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}